package pl.codementors.imiona;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UsersMain {

    public static void main(String[] args) {
        System.out.println("Hello Wrold!");
        List<User> users = load("UserList"); //loading or opening referenced list below
        printall(users);


        }


    private static void printall(Collection users) {
    for (Object user : users) {
        System.out.println(user);
    }
    }

    static List<User> load(String fileName) {
        File file = new File(fileName);
        List<User> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            int index = Integer.parseInt(br.readLine());
            while (br.ready()) { //checking if file ready to read
                String name = br.readLine(); //reading each line separately
                String surname = br.readLine();
                String login = br.readLine();
                User tempUser = new User(); //creating new user from the list we are reading
                tempUser.setName(name); //reading from file: name, surname, etc
                tempUser.setSurname(surname);
                tempUser.setLogin(login);
                list.add(tempUser); //adding new user which was created from read file, adding user to list
                System.out.println("User is: " + " " + name + " " + surname + " " + login);
            }
            return list;
        } catch (IOException ex) { //check if exceptions are in the file
            System.err.println(ex);
        }
        return null;
    }

}



